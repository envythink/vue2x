function Timer() {
  this.id =1;

  var _this = this;
  setTimeout(function () {
      console.log(this.id);  //当前方法对象，结果为：undefined
      console.log(_this.id);  //原来的对象，结果为：1
  },1000);

  setTimeout(()=>{
      console.log(this.id);  //原来的对象，结果为：1
  },2000);
}

var timer = new Timer();
