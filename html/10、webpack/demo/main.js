//导入Vue框架
import Vue from 'Vue';
//导入HelloWorld.vue组件
import HelloWorld from './HelloWorld.vue';

//创建Vue实例
new Vue({
    el: "#app",
    render: h => h(HelloWorld)
});
