/**
 * 此处仅仅是演示，将数据存放到一个列表，包含商品id、商品名称、单价、购买数量等;
 * 在实际业务中，这些数据应当是通过Ajax从服务端动态获取的。
 * **/


var app = new Vue({
    el:"#app",
    data:{
        list:[
            {
                id:1,
                name:'iphone7',
                price:6188,
                count:1
            },
                  {
                id:2,
                name:'iPad Pro',
                price:5888,
                count:1
            },
                  {
                id:3,
                name:'MacBook Pro',
                price:21488,
                count:1
            },
                  {
                id:4,
                name:'iphone11',
                price:8188,
                count:1
            },
        ]
    },
    computed:{
        Price:function () {

        },
    },
    methods:{
        handleReduce:function (index) {
            if(this.list[index].count===1){
                return;
            }
            this.list[index].count--;
        },
        handleAdd:function (index) {
            this.list[index].count++;
        },
        handleRemove:function (index) {
            this.list.splice(index,1)
        },
        selectAll:function () {
            var counter =0;
            for (var i =0;i<this.list.length;i++){
                counter +=this.list[i].price * this.list[i].count;
            }
            return counter.toString().replace(/\B(?=(\d{3})+$)/g,',');
        }
    }
});

/**
 *
 * 尽管我们在button中绑定了disabled特性，但是在handleReduce中有判断了一遍，这是因为在某些时候，可能不会使用到button元素，
 * 有可能是其他的元素，如div、span等，而给他们增加disabled属性是没有任何作用的，所以为了安全起见，我们在业务逻辑中再判断一次，避免出现修改HTML模板出现的bug。
 * **/