var path = require('path');
var webpack = require('webpack');
var MiniCssExtractPlugin  = require('mini-css-extract-plugin');
var VueLoaderPlugin = require('vue-loader/lib/plugin');
var htmlWebpackPlugin = require('html-webpack-plugin');
var merge = require('webpack-merge');
var webpackBaseConfig = require('./webpack.config.js');
var UglifyJsPlugin=require('uglifyjs-webpack-plugin');


//清空基本配置的插件列表
webpackBaseConfig.plugins = [];

module.exports = merge(webpackBaseConfig, {
    output: {
        path: path.join(__dirname, './dist'),
        publicPath: '/dist/',
        //将入口文件重命名为带有20位hash值的唯一文件
        filename: '[name].[hash].js'
    },
    plugins: [
        //提取css,且重命令为带有20位hash值的唯一文件
        new MiniCssExtractPlugin({
            filename: "[name].[hash].css",
            allChunks: true
        }),
        new VueLoaderPlugin(),
        //定义当前node环境为生产环境
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        //提取模板，并保存入口html文件
        new htmlWebpackPlugin({
            filename: '../index_prod.html',
            template: './index.ejs',
            inject: false
        })
    ],
    optimization:{
        minimizer:[
            new UglifyJsPlugin({
                uglifyOptions: {
                    output: {
                        comments: false
                    },
                    compress: {
                        drop_debugger: true,
                        drop_console: true
                    }
                }
            })
        ]
    }
});
