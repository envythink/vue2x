//导入Vue框架
import Vue from 'Vue';
//导入HelloWorld.vue组件
import HelloWorld from './HelloWorld.vue';
//导入vue-router插件
import VueRouter from 'vue-router';
//导入app.vue组件
import App from './app.vue';
//导入vuex插件
import Vuex from 'vuex';
//导入vue-bus插件
import VueBus from './vue-bus/vue-bus';


//使用vue-router插件
Vue.use(VueRouter);

const Routers = [
    {
        path: '/index',
        meta: {
            title: '首页'
        }
    },
    {
        path: '/about',
        meta: {
            title: '关于'
        },
        component:(resolve) => require(['./router/views/about.vue'], resolve)
    },
    {
        path: '/user/:id',
        meta: {
            title: '个人主页'
        },
        component:(resolve) => require(['./router/views/user.vue'], resolve)
    },
    {
        path: '*',
        redirect: '/index'
    },
];
const RouterConfig = {
    //使用HTML5的history路由模式
    mode: 'history',
    routes: Routers
};
const router = new VueRouter(RouterConfig);
router.beforeEach((to,from,next) => {
    window.document.title = to.meta.title;
    next();
});
router.afterEach((to,from,next)=> {
    window.scrollTo(0,0);
});
// router.beforeEach((to,from,next)=> {
//     if(window.localStorage.getItem("token")){
//         next();
//     }else{
//         next('/login');
//     }
// });

//使用vue插件
Vue.use(Vuex);

// const store = new Vuex.Store({
//     //vuex的配置
//     state: {
//         count: 0
//     },
//     //第一种方法
//     // mutations: {
//     //     increment (state, n=1) {
//     //         state.count+=n;
//     //     },
//     //     decrement (state, n=1) {
//     //         state.count-=n;
//     //     }
//     // }
//
//     //第二种方法
//     mutations: {
//         increment (state, params) {
//             state.count+=params.count;
//         },
//         decrement (state, params) {
//             state.count-=params.count;
//         }
//     }
// });

// const store = new Vuex.Store({
//     state: {
//         list: [1,5,8,10,30,50]
//     }
// });

//使用getters改写
// const store = new Vuex.Store({
//     state: {
//         list: [1,5,8,10,30,50]
//     },
//     getters: {
//         filteredList: state => {
//             return state.list.filter(item => item<10);
//         }
//     }
// });

//使用getter计算出list过滤后的元素的数量
// const store = new Vuex.Store({
//     state: {
//         list: [1,5,8,10,30,50]
//     },
//     getters: {
//         filteredList: state => {
//             return state.list.filter(item => item<10);
//         },
//         listCount: (state, getters) => {
//             return getters.filteredList.length;
//         }
//     }
// });


//使用action来修改数据
// const store = new Vuex.Store({
//     state: {
//         count: 0,
//     },
//     mutations: {
//         increment (state, n=1) {
//             state.count +=n;
//         }
//     },
//     actions: {
//         increment (context) {
//             context.commit('increment');
//         }
//     }
// });


//使用actions来异步提交数据,使用Promise
// const store = new Vuex.Store({
//     state: {
//         count: 0,
//     },
//     mutations: {
//         increment (state, n=1) {
//             state.count +=n;
//         }
//     },
//     actions: {
//         asyncIncrement (context) {
//             return new Promise(resolve =>{
//                 setTimeout(
//                     ()=>{
//                         context.commit('increment');
//                         resolve();
//                     },1000)
//             })
//         }
//     }
// });

//使用actions来异步提交数据,使用普通回调
const store = new Vuex.Store({
    state: {
        count: 0,
    },
    mutations: {
        increment (state, n=1) {
            state.count +=n;
        }
    },
    actions: {
        asyncIncrement (context, callback) {
            setTimeout(()=>{
                context.commit('increment');
                callback();
            },1000)
        }
    }
});

//使用vue-bus插件
Vue.use(VueBus);

//创建Vue实例
new Vue({
    el: "#app",
    //使用router
    router: router,
    //使用store
    store: store,
    render: h => h(App)
});
